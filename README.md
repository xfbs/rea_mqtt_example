# REA MQTT examples

## Install mosquitto

Use your package manager to install `mosquitto`. 

    # macOS
    brew install mosquitto

![macOS mosquitto installation](mosquitto-install.png)

## Configure mosquitto

Read and customise the mosquitto config file. See `mosquitto/mosquitto.conf` for a working config file. Typically, the system-wide config file will be located at `/etc/mosquitto/mosquitto.conf`, but when installed with Homebrew under macOS, the config file will be found at `/usr/local/etc/mosquitto/mosquitto.conf`.

## Run mosquitto

Mosquitto can be launched as a service by systemd (on Linux) or as a homebrew service (on macOS). However, it can also be launched manually.

    # macOS
    /usr/local/sbin/mosquitto -c /path/to/config.conf
    # Linux
    /usr/sbin/mosquitto -c /path/to/config.conf

Mosquitto should (if configured to log on stdout) output some information on stdout.

    1572175485: mosquitto version 1.6.7 starting
    1572175485: Config loaded from mosquitto/mosquitto.conf.
    1572175485: Opening ipv6 listen socket on port 1883.
    1572175485: Opening ipv4 listen socket on port 1883.

## Ruby Example

There is a Ruby gem which provides native MQTT client implementation. Install this with rubygems.

    gem install mqtt

Run the example in `ruby/quick_start.rb`. This will connect twice to the MQTT broker and send messages back and forth.

    $ ruby ruby/quick_start.rb
    test: message
    test: message
    test: message
    ...

## C Example

There is an example of a sender and a receiver written in C. These use `libmosquitto`, which is shipped with `mosquitto` and provides an easy-to-use C API for connecting to and interacting with MQTT servers.

This can be compiled using the provided `Makefile` with

    $ make -C c

which will place the resulting binaries into the `c/` folder, alongside the source code.

These can be run like such.

    $ ./c/sender
    [in another terminal]
    $ ./c/receiver
    [test] hello world
    [test] hello world
    [test] hello world
    ...

The sender will simply send a preconfigured message on a preconfigured topic 100 times and disconnect, while the receiver subscribes to the topic and outputs any received messages.

Note that the receiver has to be started *before* the sender — otherwise, the messages will be silently dropped.

### Speed Test

In order to get a rough estimate of the speed of MQTT, I have implemented a pair of small C programs which will read from *stdin*, send it to the MQTT broker, and (the other program) wait for messages from the broker and push them out on *stdout*. This can be used to check the speed, using something like `pv`.

    $ ./c/stdin-sender < /dev/urandom
    [in another terminal]
    $ ./c/stdout-writer | pv > /dev/null

On my local machine, I get around 450 MB/s, which isn't too bad (in the same general ballpark, maybe a tad bit slower, than UNIX pipes).

![MQTT speed on my local machine](mqtt-speed.png)

This same setup can be used to check for the integrity of the messages read. Using it as an integrity check on my local system shows no issues.

![MQTT integrity check matching](mqtt-integrity.png)

As you can see, the SHA256 hash sums match, showing that the data was not altered during transit.

## NodeJS Example

There is also an example code to send and receive MQTT messages using nodejs. To get started, you need to first install the `mqtt` package from the NPM registry.

    npm install -C nodejs mqtt --save

Next, you can run the scripts.

    $ node nodejs/sender.js
    [in another terminal]
    $ node nodejs/receiver.js
    [test] hello world
    [test] hello world
    [test] hello world
    ...

The sender and receiver work in much the same way as the C versions do.

## ToDo

Encryption, access control?
