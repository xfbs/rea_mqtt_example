#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>
#include <assert.h>

#define TOPIC "test"

void on_message(
        struct mosquitto *mosq,
        void *data,
        const struct mosquitto_message *msg)
{
    (void)data;
    (void)mosq;
    printf("[%s] %s\n", msg->topic, (char *)msg->payload);
}

int main(int argc, char *argv[]) {
    const char *host = NULL;
    size_t port = 0;
    int ret;

    /* parse command-line arguments to get host and port */
    switch(argc) {
        case 3:
            port = atoi(argv[2]);
        case 2:
            host = argv[1];
        case 1:
            if(!port) port = 1883;
            if(!host) host = "localhost";
            break;
        default:
            assert(false);
    }

    /* create new mosquitto client instance */
    struct mosquitto *mosq = mosquitto_new(
            NULL,
            true,
            NULL);
    assert(mosq);

    /* connect to mosquitto server */
    ret = mosquitto_connect(
            mosq,
            host,
            port,
            25);
    assert(MOSQ_ERR_SUCCESS == ret);

    /* subscribe to a topic */
    ret = mosquitto_subscribe(
            mosq,
            NULL,
            TOPIC,
            0);
    assert(MOSQ_ERR_SUCCESS == ret);

    /* set handler to be called on receiving a message */
    mosquitto_message_callback_set(
            mosq,
            on_message);

    /* start event loop, will block until mosquitto_disconnect() is called in
     * any of the callbacks. */
    mosquitto_loop_forever(
            mosq,
            -1,
            1);
    assert(MOSQ_ERR_SUCCESS == ret);

    mosquitto_disconnect(mosq);

    return 0;
}
