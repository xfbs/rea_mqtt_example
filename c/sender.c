#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>
#include <assert.h>

#define MESSAGE "hello world"
#define TOPIC "test"

int main(int argc, char *argv[]) {
    const char *host = NULL;
    size_t port = 0;
    int ret;

    /* parse command-line arguments to get host and port */
    switch(argc) {
        case 3:
            port = atoi(argv[2]);
        case 2:
            host = argv[1];
        case 1:
            if(!port) port = 1883;
            if(!host) host = "localhost";
            break;
        default:
            assert(false);
    }

    /* create new mosquitto client instance */
    struct mosquitto *mosq = mosquitto_new(
            NULL,
            true,
            NULL);
    assert(mosq);

    /* connect to mosquitto server */
    ret = mosquitto_connect(
            mosq,
            host,
            port,
            25);
    assert(MOSQ_ERR_SUCCESS == ret);

    /* send message one hundred times */
    for(size_t i = 0; i < 100; i++) {
        ret = mosquitto_publish(
                mosq,
                NULL,
                TOPIC,
                sizeof(MESSAGE),
                MESSAGE,
                0,
                false);
        assert(MOSQ_ERR_SUCCESS == ret);
    }

    mosquitto_disconnect(mosq);

    return 0;
}
