#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>
#include <assert.h>

#define TOPIC "stream"

int main(int argc, char *argv[]) {
    const char *host = NULL;
    size_t port = 0;
    int ret;

    /* parse command-line arguments to get host and port */
    switch(argc) {
        case 3:
            port = atoi(argv[2]);
        case 2:
            host = argv[1];
        case 1:
            if(!port) port = 1883;
            if(!host) host = "localhost";
            break;
        default:
            assert(false);
    }

    /* create new mosquitto client instance */
    struct mosquitto *mosq = mosquitto_new(
            NULL,
            true,
            NULL);
    assert(mosq);

    /* connect to mosquitto server */
    ret = mosquitto_connect(
            mosq,
            host,
            port,
            25);
    assert(MOSQ_ERR_SUCCESS == ret);

    /* send message one hundred times */
    while(!feof(stdin) && !ferror(stdin)) {
        char buffer[1024 * 1024];
        size_t read = fread(buffer, 1, sizeof(buffer), stdin);

        if(read) {
            ret = mosquitto_publish(
                    mosq,
                    NULL,
                    TOPIC,
                    read,
                    buffer,
                    0,
                    false);
            assert(MOSQ_ERR_SUCCESS == ret);
        }

        if(feof(stdin) || ferror(stdin)) {
            break;
        }
    }

    mosquitto_disconnect(mosq);

    return 0;
}
