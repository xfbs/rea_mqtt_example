var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost')

// connect to server
client.on('connect', function () {
  // subscribe to topic
  client.subscribe('test', function (err) {
    if (!err) {
      // handle message
      client.on('message', function (topic, message) {
        // print out topic and message
        console.log(`[${topic}] ${message.toString()}`)
      })
    }
  })
})
