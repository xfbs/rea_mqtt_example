var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://localhost')

// javascript doesn't have a neat syntactic sugar like ruby's
//
// 100.times do
//   ...
// end
//
// so here I do a little bit of a hack to give a similar kind of
// syntax. use this like
//
// times (n) (() => some_function())
//
// to run some_function() n times.
const times = x => f => {
  if (x > 0) {
    f()
    times (x - 1) (f)
  }
}

client.on('connect', function () {
  // send the message out 100 times
  times (100) (() => client.publish('test', 'hello world'))

  // disconnect, this stops the event loop.
  client.end()
})
