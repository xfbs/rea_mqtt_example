require 'rubygems'
require 'mqtt'

SERVER = ARGV[0] || 'localhost'

# listen to incoming messages and print them out.
MQTT::Client.connect(SERVER) do |c|
  c.get('test') do |topic, message|
    puts "#{topic}: #{message}"
  end
end
