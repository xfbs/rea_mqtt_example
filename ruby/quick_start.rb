require 'rubygems'
require 'mqtt'

SERVER = ARGV[0] || 'localhost'

# start a new thread, sending 100 messages (one every second).
Thread.new do
  MQTT::Client.connect(SERVER) do |c|
    100.times do
      c.publish('test', 'message')
      sleep 1
    end
  end
end

# listen to incoming messages and print them out.
MQTT::Client.connect(SERVER) do |c|
  c.get('test') do |topic, message|
    puts "#{topic}: #{message}"
  end
end
